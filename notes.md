# LOG8415E Cloud computing #

First, who is running which instance :
- m4.large => Hooman/Mohab?
- c5.xlarge => Rym
- c5.2xlarge => Hooman/Mohab?
- r5.large => Hooman/Mohab?
- h1.2xlarge => Rym

HOW TO RUN THE SCRIPTS :

=> git pull
-> will update your files on the instance
OR
git clone https://d_olewicki@bitbucket.org/d_olewicki/log8415-tp1.git
-> will download the git

=> sudo apt-get ...
-> update everything, benchmarkers included

=> python3 do_*name_of_bench*.py -m *name_of_machine*

copy the results in a out_*name_of_machine*.txt file (copy/past for now, I'll change the file so that it is automatic)

report the results in a exel graph!

=> git add out_*name_of_machine*.txt
-> will transfer will save the modification on your local git

=> git commit -m "done with output of *name_of_machine*"
-> create the commit

=> git push
-> send the commit on line



Here are some information on how to use each benchmark asked in the lab.

##### Sysbench : ##### => Doriane
You need to install sysbench
-> sudo apt-get install sysbench

sysbench --test=cpu --cpu-max-prime=1000000 --events=45000 --time=240 --num_threads=4000 run

=> need to vary the --num_threads

#lower bound : --num_threads=1
#upper bound : --num_threads=7900

checkout :
- https://www.howtoforge.com/how-to-benchmark-your-system-cpu-file-io-mysql-with-sysbench
- http://imysql.com/wp-content/uploads/2014/10/sysbench-manual.pdf

##### dd : ##### => Rym
dd if=/dev/zero of=sb-io-test bs=1M count=1k conv=fdatasync
=> vary bs for blocksize and count for the number of blocks

#lower bound : bs=1 count=0
#lower upper bound : bs=1M count=6k (6500)
#upper bound : bs=1M count=6k

checkout :
- https://medium.com/@kenichishibata/test-i-o-performance-of-linux-using-dd-a5074f1de9ce

##### Bonnie++ : ##### => Hooman, TODO Mohab
You need to install bonnnie++
-> sudo apt-get install bonnie++

bonnie++ -r 1024 -s 3250 -u ubuntu
=> -s SIZE : SIZE should be higher than 2xRAM (-r) (at least 2x7972MB). It is in MB
=> vary -r and -s, find upper bound (lower than 2048...)

#lower bound : -r 2 -s 4
#upper bound : -r 2048 -s 6075
#upper bound : -r 2048 -s 6500

checkout :
- https://www.jamescoyle.net/how-to/599-benchmark-disk-io-with-dd-and-bonnie

##### stress-ng ##### => TODO Hooman
You need to install stress-ng
-> sudo apt-get install stress-ng

stress-ng --cpu 4 --io 2 --vm 1 --vm-bytes 1 --timeout 60s --metrics-brief

stress-ng --cpu 4 --io 2 --vm 8192 --vm-bytes 35G --timeout 60s --metrics-brief
=> vary --vm and --vm-bytes for the memory

#lower bound : --vm 1 --vm-bytes 4k
#lower upper bound : --vm 8192 --vm-bytes 59G
#upper bound : --vm 8192 --vm-bytes 125G

checkout :
- http://manpages.ubuntu.com/manpages/bionic/man1/stress-ng.1.html

##### hdparm ##### => TODO Doriane
You need to install hdparm
-> sudo apt-get install hdparm

sudo hdparm -Tt /dev/xvda
=> is xvda is not found, check for sda

checkout :
- https://linux.die.net/man/8/hdparm

##### SpeedTest ##### => TODO Rym
You need to install speedtest
-> sudo apt-get install speedtest-cli

speedtest-cli

checkout :
- https://www.howtoforge.com/tutorial/check-internet-speed-with-speedtest-cli-on-ubuntu/?fbclid=IwAR10TmENJnwX9d-KRecIJDw5D6kE_Iw0JY6WdzJw-iYTXGQgtttAE5BnrX0
