RAM 2 -- dataset 4
==================
Using uid:1000, gid:1000.
Writing a byte at a time...done
Writing intelligently...done
Rewriting...done
Reading a byte at a time...done
Reading intelligently...done
start 'em...done...done...done...done...done...
Create files in sequential order...done.
Stat files in sequential order...done.
Delete files in sequential order...done.
Create files in random order...done.
Stat files in random order...done.
Delete files in random order...done.
Version  1.97       ------Sequential Output------ --Sequential Input- --Random-
Concurrency   1     -Per Chr- --Block-- -Rewrite- -Per Chr- --Block-- --Seeks--
Machine        Size K/sec %CP K/sec %CP K/sec %CP K/sec %CP K/sec %CP  /sec %CP
ip-172-31-40-176 4G   934  99 133651   8 131242   5  2491  99 5258108 100 +++++ +++
Latency              9202us   26031us   26641us    3509us      26us      45us
Version  1.97       ------Sequential Create------ --------Random Create--------
ip-172-31-40-176    -Create-- --Read--- -Delete-- -Create-- --Read--- -Delete--
              files  /sec %CP  /sec %CP  /sec %CP  /sec %CP  /sec %CP  /sec %CP
                 16 +++++ +++ +++++ +++ +++++ +++ +++++ +++ +++++ +++ +++++ +++
Latency                51us     325us     406us    1008us     159us     284us
1.97,1.97,ip-172-31-40-176,1,1549421638,4G,,934,99,133651,8,131242,5,2491,99,5258108,100,+++++,+++,16,,,,,+++++,+++,+++++,+++,+++++,+++,+++++,+++,+++++,+++,+++++,+++,9202us,26031us,26641us,3509us,26us,45us,51us,325us,406us,1008us,159us,284us

RAM 1 -- dataset 2
==================
Using uid:1000, gid:1000.
Writing a byte at a time...done
Writing intelligently...done
Rewriting...done
Reading a byte at a time...done
Reading intelligently...done
start 'em...done...done...done...done...done...
Create files in sequential order...done.
Stat files in sequential order...done.
Delete files in sequential order...done.
Create files in random order...done.
Stat files in random order...done.
Delete files in random order...done.
Version  1.97       ------Sequential Output------ --Sequential Input- --Random-
Concurrency   1     -Per Chr- --Block-- -Rewrite- -Per Chr- --Block-- --Seeks--
Machine        Size K/sec %CP K/sec %CP K/sec %CP K/sec %CP K/sec %CP  /sec %CP
ip-172-31-40-176 2G   930  99 136068   8 131232   5  2481  99 +++++ +++ +++++ +++
Latency              8687us   22021us   22627us    3517us      20us     419us
Version  1.97       ------Sequential Create------ --------Random Create--------
ip-172-31-40-176    -Create-- --Read--- -Delete-- -Create-- --Read--- -Delete--
              files  /sec %CP  /sec %CP  /sec %CP  /sec %CP  /sec %CP  /sec %CP
                 16 +++++ +++ +++++ +++ +++++ +++ +++++ +++ +++++ +++ +++++ +++
Latency                49us     319us     341us     761us      17us      33us
1.97,1.97,ip-172-31-40-176,1,1549420759,2G,,930,99,136068,8,131232,5,2481,99,+++++,+++,+++++,+++,16,,,,,+++++,+++,+++++,+++,+++++,+++,+++++,+++,+++++,+++,+++++,+++,8687us,22021us,22627us,3517us,20us,419us,49us,319us,341us,761us,17us,33us


RAM 512 -- dataset 1024
=======================
Using uid:1000, gid:1000.
Writing a byte at a time...done
Writing intelligently...done
Rewriting...done
Reading a byte at a time...done
Reading intelligently...done
start 'em...done...done...done...done...done...
Create files in sequential order...done.
Stat files in sequential order...done.
Delete files in sequential order...done.
Create files in random order...done.
Stat files in random order...done.
Delete files in random order...done.
Version  1.97       ------Sequential Output------ --Sequential Input- --Random-
Concurrency   1     -Per Chr- --Block-- -Rewrite- -Per Chr- --Block-- --Seeks--
Machine        Size K/sec %CP K/sec %CP K/sec %CP K/sec %CP K/sec %CP  /sec %CP
ip-172-31-40-176 1G   934  99 141269   9 131230   6  2508  99 +++++ +++ +++++ +++
Latency              8624us      30us      30us    3463us      24us      28us
Version  1.97       ------Sequential Create------ --------Random Create--------
ip-172-31-40-176    -Create-- --Read--- -Delete-- -Create-- --Read--- -Delete--
              files  /sec %CP  /sec %CP  /sec %CP  /sec %CP  /sec %CP  /sec %CP
                 16 +++++ +++ +++++ +++ +++++ +++ +++++ +++ +++++ +++ +++++ +++
Latency                55us     324us     351us      50us     510us      28us
1.97,1.97,ip-172-31-40-176,1,1549421377,1G,,934,99,141269,9,131230,6,2508,99,+++++,+++,+++++,+++,16,,,,,+++++,+++,+++++,+++,+++++,+++,+++++,+++,+++++,+++,+++++,+++,8624us,30us,30us,3463us,24us,28us,55us,324us,351us,50us,510us,28us


RAM 256 -- dataset 512
======================
Using uid:1000, gid:1000.
Writing a byte at a time...done
Writing intelligently...done
Rewriting...done
Reading a byte at a time...done
Reading intelligently...done
start 'em...done...done...done...done...done...
Create files in sequential order...done.
Stat files in sequential order...done.
Delete files in sequential order...done.
Create files in random order...done.
Stat files in random order...done.
Delete files in random order...done.
Version  1.97       ------Sequential Output------ --Sequential Input- --Random-
Concurrency   1     -Per Chr- --Block-- -Rewrite- -Per Chr- --Block-- --Seeks--
Machine        Size K/sec %CP K/sec %CP K/sec %CP K/sec %CP K/sec %CP  /sec %CP
ip-172-31-40-1 512M   934  99 158786  10 131237   6  2517  99 +++++ +++ +++++ +++
Latency              8587us      20us      26us    3443us      22us    7761us
Version  1.97       ------Sequential Create------ --------Random Create--------
ip-172-31-40-176    -Create-- --Read--- -Delete-- -Create-- --Read--- -Delete--
              files  /sec %CP  /sec %CP  /sec %CP  /sec %CP  /sec %CP  /sec %CP
                 16 +++++ +++ +++++ +++ +++++ +++ +++++ +++ +++++ +++ +++++ +++
Latency                46us     321us     344us    1222us      13us      30us
1.97,1.97,ip-172-31-40-176,1,1549421536,512M,,934,99,158786,10,131237,6,2517,99,+++++,+++,+++++,+++,16,,,,,+++++,+++,+++++,+++,+++++,+++,+++++,+++,+++++,+++,+++++,+++,8587us,20us,26us,3443us,22us,7761us,46us,321us,344us,1222us,13us,30us

RAM 128 -- dataset 256
======================
Using uid:1000, gid:1000.
Writing a byte at a time...done
Writing intelligently...done
Rewriting...done
Reading a byte at a time...done
Reading intelligently...done
start 'em...done...done...done...done...done...
Create files in sequential order...done.
Stat files in sequential order...done.
Delete files in sequential order...done.
Create files in random order...done.
Stat files in random order...done.
Delete files in random order...done.
Version  1.97       ------Sequential Output------ --Sequential Input- --Random-
Concurrency   1     -Per Chr- --Block-- -Rewrite- -Per Chr- --Block-- --Seeks--
Machine        Size K/sec %CP K/sec %CP K/sec %CP K/sec %CP K/sec %CP  /sec %CP
ip-172-31-40-1 256M   936  99 227164  15 131235   6  2512  99 +++++ +++ +++++ +++
Latency              8686us      22us      23us    3461us      19us      41us
Version  1.97       ------Sequential Create------ --------Random Create--------
ip-172-31-40-176    -Create-- --Read--- -Delete-- -Create-- --Read--- -Delete--
              files  /sec %CP  /sec %CP  /sec %CP  /sec %CP  /sec %CP  /sec %CP
                 16 +++++ +++ +++++ +++ +++++ +++ +++++ +++ +++++ +++ +++++ +++
Latency                45us     331us    1423us    1697us      14us      36us
1.97,1.97,ip-172-31-40-176,1,1549421523,256M,,936,99,227164,15,131235,6,2512,99,+++++,+++,+++++,+++,16,,,,,+++++,+++,+++++,+++,+++++,+++,+++++,+++,+++++,+++,+++++,+++,8686us,22us,23us,3461us,19us,41us,45us,331us,1423us,1697us,14us,36us

RAM 64 -- dataset 128
=====================
Using uid:1000, gid:1000.
Writing a byte at a time...done
Writing intelligently...done
Rewriting...done
Reading a byte at a time...done
Reading intelligently...done
start 'em...done...done...done...done...done...
Create files in sequential order...done.
Stat files in sequential order...done.
Delete files in sequential order...done.
Create files in random order...done.
Stat files in random order...done.
Delete files in random order...done.
Version  1.97       ------Sequential Output------ --Sequential Input- --Random-
Concurrency   1     -Per Chr- --Block-- -Rewrite- -Per Chr- --Block-- --Seeks--
Machine        Size K/sec %CP K/sec %CP K/sec %CP K/sec %CP K/sec %CP  /sec %CP
ip-172-31-40-1 128M   933  99 +++++ +++ 178462   8  2490  99 +++++ +++ +++++ +++
Latency              8651us      30us      28us    3494us      20us      37us
Version  1.97       ------Sequential Create------ --------Random Create--------
ip-172-31-40-176    -Create-- --Read--- -Delete-- -Create-- --Read--- -Delete--
              files  /sec %CP  /sec %CP  /sec %CP  /sec %CP  /sec %CP  /sec %CP
                 16 +++++ +++ +++++ +++ +++++ +++ +++++ +++ +++++ +++ +++++ +++
Latency               816us     326us     344us     751us      64us    1465us
1.97,1.97,ip-172-31-40-176,1,1549421174,128M,,933,99,+++++,+++,178462,8,2490,99,+++++,+++,+++++,+++,16,,,,,+++++,+++,+++++,+++,+++++,+++,+++++,+++,+++++,+++,+++++,+++,8651us,30us,28us,3494us,20us,37us,816us,326us,344us,751us,64us,1465us

RAM 32 -- dataset 64
====================
Using uid:1000, gid:1000.
Writing a byte at a time...done
Writing intelligently...done
Rewriting...done
Reading a byte at a time...done
Reading intelligently...done
start 'em...done...done...done...done...done...
Create files in sequential order...done.
Stat files in sequential order...done.
Delete files in sequential order...done.
Create files in random order...done.
Stat files in random order...done.
Delete files in random order...done.
Version  1.97       ------Sequential Output------ --Sequential Input- --Random-
Concurrency   1     -Per Chr- --Block-- -Rewrite- -Per Chr- --Block-- --Seeks--
Machine        Size K/sec %CP K/sec %CP K/sec %CP K/sec %CP K/sec %CP  /sec %CP
ip-172-31-40-17 64M   935  99 +++++ +++ +++++ +++  2522  99 +++++ +++ +++++ +++
Latency              8623us      21us      28us    3454us      25us    4029us
Version  1.97       ------Sequential Create------ --------Random Create--------
ip-172-31-40-176    -Create-- --Read--- -Delete-- -Create-- --Read--- -Delete--
              files  /sec %CP  /sec %CP  /sec %CP  /sec %CP  /sec %CP  /sec %CP
                 16 +++++ +++ +++++ +++ +++++ +++ +++++ +++ +++++ +++ +++++ +++
Latency               154us     322us     343us      45us      13us     990us
1.97,1.97,ip-172-31-40-176,1,1549421126,64M,,935,99,+++++,+++,+++++,+++,2522,99,+++++,+++,+++++,+++,16,,,,,+++++,+++,+++++,+++,+++++,+++,+++++,+++,+++++,+++,+++++,+++,8623us,21us,28us,3454us,25us,4029us,154us,322us,343us,45us,13us,990us

RAM 16 -- dataset 32
====================
Using uid:1000, gid:1000.
Writing a byte at a time...done
Writing intelligently...done
Rewriting...done
Reading a byte at a time...done
Reading intelligently...done
start 'em...done...done...done...done...done...
Create files in sequential order...done.
Stat files in sequential order...done.
Delete files in sequential order...done.
Create files in random order...done.
Stat files in random order...done.
Delete files in random order...done.
Version  1.97       ------Sequential Output------ --Sequential Input- --Random-
Concurrency   1     -Per Chr- --Block-- -Rewrite- -Per Chr- --Block-- --Seeks--
Machine        Size K/sec %CP K/sec %CP K/sec %CP K/sec %CP K/sec %CP  /sec %CP
ip-172-31-40-17 32M   937  99 +++++ +++ +++++ +++  2490  99 +++++ +++ +++++ +++
Latency              8643us      17us      27us    3476us      20us      31us
Version  1.97       ------Sequential Create------ --------Random Create--------
ip-172-31-40-176    -Create-- --Read--- -Delete-- -Create-- --Read--- -Delete--
              files  /sec %CP  /sec %CP  /sec %CP  /sec %CP  /sec %CP  /sec %CP
                 16 +++++ +++ +++++ +++ +++++ +++ +++++ +++ +++++ +++ +++++ +++
Latency                42us     329us     349us    4056us      13us      28us
1.97,1.97,ip-172-31-40-176,1,1549421291,32M,,937,99,+++++,+++,+++++,+++,2490,99,+++++,+++,+++++,+++,16,,,,,+++++,+++,+++++,+++,+++++,+++,+++++,+++,+++++,+++,+++++,+++,8643us,17us,27us,3476us,20us,31us,42us,329us,349us,4056us,13us,28us

RAM 8 -- dataset 16
===================
Using uid:1000, gid:1000.
Writing a byte at a time...done
Writing intelligently...done
Rewriting...done
Reading a byte at a time...done
Reading intelligently...done
start 'em...done...done...done...done...done...
Create files in sequential order...done.
Stat files in sequential order...done.
Delete files in sequential order...done.
Create files in random order...done.
Stat files in random order...done.
Delete files in random order...done.
Version  1.97       ------Sequential Output------ --Sequential Input- --Random-
Concurrency   1     -Per Chr- --Block-- -Rewrite- -Per Chr- --Block-- --Seeks--
Machine        Size K/sec %CP K/sec %CP K/sec %CP K/sec %CP K/sec %CP  /sec %CP
ip-172-31-40-17 16M   934  99 +++++ +++ +++++ +++  2510  99 +++++ +++ +++++ +++
Latency              8638us      17us      27us    3396us      20us      55us
Version  1.97       ------Sequential Create------ --------Random Create--------
ip-172-31-40-176    -Create-- --Read--- -Delete-- -Create-- --Read--- -Delete--
              files  /sec %CP  /sec %CP  /sec %CP  /sec %CP  /sec %CP  /sec %CP
                 16 +++++ +++ +++++ +++ +++++ +++ +++++ +++ +++++ +++ +++++ +++
Latency               445us     326us    1308us      44us    1090us      31us
1.97,1.97,ip-172-31-40-176,1,1549421082,16M,,934,99,+++++,+++,+++++,+++,2510,99,+++++,+++,+++++,+++,16,,,,,+++++,+++,+++++,+++,+++++,+++,+++++,+++,+++++,+++,+++++,+++,8638us,17us,27us,3396us,20us,55us,445us,326us,1308us,44us,1090us,31us

RAM 4 -- dataset 8
==================
Using uid:1000, gid:1000.
Writing a byte at a time...done
Writing intelligently...done
Rewriting...done
Reading a byte at a time...done
Reading intelligently...done
start 'em...done...done...done...done...done...
Create files in sequential order...done.
Stat files in sequential order...done.
Delete files in sequential order...done.
Create files in random order...done.
Stat files in random order...done.
Delete files in random order...done.
Version  1.97       ------Sequential Output------ --Sequential Input- --Random-
Concurrency   1     -Per Chr- --Block-- -Rewrite- -Per Chr- --Block-- --Seeks--
Machine        Size K/sec %CP K/sec %CP K/sec %CP K/sec %CP K/sec %CP  /sec %CP
ip-172-31-40-176 8M   936  99 +++++ +++ +++++ +++  2555  99 +++++ +++ +++++ +++
Latency              8606us      41us      25us    3380us       7us      31us
Version  1.97       ------Sequential Create------ --------Random Create--------
ip-172-31-40-176    -Create-- --Read--- -Delete-- -Create-- --Read--- -Delete--
              files  /sec %CP  /sec %CP  /sec %CP  /sec %CP  /sec %CP  /sec %CP
                 16 +++++ +++ +++++ +++ +++++ +++ +++++ +++ +++++ +++ +++++ +++
Latency                43us     325us    1019us    1371us      15us      30us
1.97,1.97,ip-172-31-40-176,1,1549421129,8M,,936,99,+++++,+++,+++++,+++,2555,99,+++++,+++,+++++,+++,16,,,,,+++++,+++,+++++,+++,+++++,+++,+++++,+++,+++++,+++,+++++,+++,8606us,41us,25us,3380us,7us,31us,43us,325us,1019us,1371us,15us,30us

RAM 2 -- dataset 4
==================
Using uid:1000, gid:1000.
Writing a byte at a time...done
Writing intelligently...done
Rewriting...done
Reading a byte at a time...done
Reading intelligently...done
start 'em...done...done...done...done...done...
Create files in sequential order...done.
Stat files in sequential order...done.
Delete files in sequential order...done.
Create files in random order...done.
Stat files in random order...done.
Delete files in random order...done.
Version  1.97       ------Sequential Output------ --Sequential Input- --Random-
Concurrency   1     -Per Chr- --Block-- -Rewrite- -Per Chr- --Block-- --Seeks--
Machine        Size K/sec %CP K/sec %CP K/sec %CP K/sec %CP K/sec %CP  /sec %CP
ip-172-31-40-176 4M   933  99 +++++ +++ +++++ +++  2520  99 +++++ +++ +++++ +++
Latency              8608us      12us      12us    3460us       3us    3238us
Version  1.97       ------Sequential Create------ --------Random Create--------
ip-172-31-40-176    -Create-- --Read--- -Delete-- -Create-- --Read--- -Delete--
              files  /sec %CP  /sec %CP  /sec %CP  /sec %CP  /sec %CP  /sec %CP
                 16 +++++ +++ +++++ +++ +++++ +++ +++++ +++ +++++ +++ +++++ +++
Latency                44us     334us     349us    4172us      13us      28us
1.97,1.97,ip-172-31-40-176,1,1549419924,4M,,933,99,+++++,+++,+++++,+++,2520,99,+++++,+++,+++++,+++,16,,,,,+++++,+++,+++++,+++,+++++,+++,+++++,+++,+++++,+++,+++++,+++,8608us,12us,12us,3460us,3us,3238us,44us,334us,349us,4172us,13us,28us
