sysbench --test=cpu --cpu-max-prime=1000000 --events=45000 --time=240 --num_threads=2000 run
WARNING: the --test option is deprecated. You can pass a script name or path on the command line without any options.
WARNING: --num-threads is deprecated, use --threads instead
sysbench 1.0.11 (using system LuaJIT 2.1.0-beta3)

Running the test with following options:
Number of threads: 2000
Initializing random number generator from current time


Prime numbers limit: 1000000

Initializing worker threads...

Threads started!

CPU speed:
    events per second:    12.57

General statistics:
    total time:                          318.2044s
    total number of events:              4000

Latency (ms):
         min:                             121123.19
         avg:                             154829.33
         max:                             163744.34
         95th percentile:                 100000.00
         sum:                            619317337.00

Threads fairness:
    events (avg/stddev):           2.0000/0.00
    execution time (avg/stddev):   309.6587/5.53



sysbench --test=cpu --cpu-max-prime=1000000 --events=45000 --time=240 --num_threads=2000 run
WARNING: the --test option is deprecated. You can pass a script name or path on the command line without any options.
WARNING: --num-threads is deprecated, use --threads instead
sysbench 1.0.11 (using system LuaJIT 2.1.0-beta3)

Running the test with following options:
Number of threads: 2000
Initializing random number generator from current time


Prime numbers limit: 1000000

Initializing worker threads...

Threads started!

CPU speed:
    events per second:    12.57

General statistics:
    total time:                          318.1578s
    total number of events:              4000

Latency (ms):
         min:                             101724.97
         avg:                             154674.83
         max:                             168604.24
         95th percentile:                 100000.00
         sum:                            618699332.58

Threads fairness:
    events (avg/stddev):           2.0000/0.00
    execution time (avg/stddev):   309.3497/4.22



sysbench --test=cpu --cpu-max-prime=1000000 --events=45000 --time=240 --num_threads=2000 run
WARNING: the --test option is deprecated. You can pass a script name or path on the command line without any options.
WARNING: --num-threads is deprecated, use --threads instead
sysbench 1.0.11 (using system LuaJIT 2.1.0-beta3)

Running the test with following options:
Number of threads: 2000
Initializing random number generator from current time


Prime numbers limit: 1000000

Initializing worker threads...

Threads started!

CPU speed:
    events per second:    12.57

General statistics:
    total time:                          318.1985s
    total number of events:              4000

Latency (ms):
         min:                             107754.88
         avg:                             153092.97
         max:                             170064.34
         95th percentile:                 100000.00
         sum:                            612371891.62

Threads fairness:
    events (avg/stddev):           2.0000/0.00
    execution time (avg/stddev):   306.1859/11.30



sysbench --test=cpu --cpu-max-prime=1000000 --events=45000 --time=240 --num_threads=2000 run
WARNING: the --test option is deprecated. You can pass a script name or path on the command line without any options.
WARNING: --num-threads is deprecated, use --threads instead
sysbench 1.0.11 (using system LuaJIT 2.1.0-beta3)

Running the test with following options:
Number of threads: 2000
Initializing random number generator from current time


Prime numbers limit: 1000000

Initializing worker threads...

Threads started!

CPU speed:
    events per second:    12.57

General statistics:
    total time:                          318.3339s
    total number of events:              4000

Latency (ms):
         min:                             120981.01
         avg:                             154234.68
         max:                             171272.71
         95th percentile:                 100000.00
         sum:                            616938722.70

Threads fairness:
    events (avg/stddev):           2.0000/0.00
    execution time (avg/stddev):   308.4694/5.17



sysbench --test=cpu --cpu-max-prime=1000000 --events=45000 --time=240 --num_threads=2000 run
WARNING: the --test option is deprecated. You can pass a script name or path on the command line without any options.
WARNING: --num-threads is deprecated, use --threads instead
sysbench 1.0.11 (using system LuaJIT 2.1.0-beta3)

Running the test with following options:
Number of threads: 2000
Initializing random number generator from current time


Prime numbers limit: 1000000

Initializing worker threads...

Threads started!

CPU speed:
    events per second:    12.57

General statistics:
    total time:                          318.2097s
    total number of events:              4000

Latency (ms):
         min:                             112237.20
         avg:                             155082.30
         max:                             166540.17
         95th percentile:                 100000.00
         sum:                            620329193.47

Threads fairness:
    events (avg/stddev):           2.0000/0.00
    execution time (avg/stddev):   310.1646/4.09
