import os, sys
import numpy as np
import math
import re

import subprocess
import csv

import argparse


parser = argparse.ArgumentParser()
parser.add_argument("-m", "--machine", help="Name of the machine", required=False)
args = parser.parse_args()

if args.machine:
    machine = args.machine
    machine = re.sub(r"\.", r"_", machine)
else:
    nbr = input("What machine our you working on ? \n \t1 - m4.large\n \t2 - c5.xlarge\n \t3 - c5.2xlarge\n \t4 - r5.large\n \t5 - h1.2xlarge\n (Write the number) ")

    machines = ["m4_large", "c5_xlarge", "c5_2xlarge", "r5_large", "h1_2xlarge"]
    machine = machines[int(nbr)-1]
print(machine)

iter = 5

##### LAUNCH_BENCHMARKING #####
# function to launch the benchmark.
# the results will be stocked in a csv file of name *out_filename*.csv
# test_set should be a list of list/elements representing the parameters variations
# for your benchmark tool. each element of the is given as an parameter to your
# cmd function.
# The cmd function takes one argument (the parameters variation, as a element or
# a list).
# iter is a integer saying how many iteration the benchmark should be done.

def launch_benchmarking(out_filename, test_set, iter, cmd, process, colnames):
    with open(out_filename + ".csv", mode='w') as res:
        res_writer = csv.writer(res, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        res_writer.writerow(colnames)

        for param in test_set:
            for i in range(iter):
                try:
                    print("\n")
                    print(cmd(param))
                    # os.system(cmd(param))
                    result = subprocess.check_output(cmd(param), stderr=subprocess.STDOUT, shell=True)
                    process(param, result, res_writer)
                except:
                    print(["ERROR", out_filename, param])
