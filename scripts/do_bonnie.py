from benchmark import *

##### BONNIE++ #####
bonnie_filename = 'bonnie' + '_' + machine
bonnie_colnames = ['RAM', 'datablock_size',
        'SeqOutPerChar(K/sec)', 'SeqOutPerChar(%CPU)', 'SeqOutBlock(K/sec)', 'SeqOutBlock(%CPU)','SeqOutRewrite(K/sec)', 'SeqOutRewrite(%CPU)',
        'SeqInPerChar(K/sec)', 'SeqInPerChar(%CPU)', 'SeqInBlock(K/sec)', 'SeqInBlock(%CPU)','RandSeek(/sec)', 'RandSeek(%CPU)',
        'SeqCreateCreate(/sec)','SeqCreateCreate(%CPU)','SeqCreateRead(/sec)','SeqCreateRead(%CPU)','SeqCreateDelete(/sec)','SeqCreateDelete(%CPU)',
        'RandCreateCreate(/sec)','RandCreateCreate(%CPU)','RandCreateRead(/sec)','RandCreateRead(%CPU)','RandCreateDelete(/sec)','RandCreateDelete(%CPU)',
        'LatSeqOutPerChar(us)', 'LatSeqOutBlock(us)', 'LatSeqOutRewrite(us)',
        'LatSeqInPerChar(us)', 'LatSeqInBlock(us)', 'LatRandSeek(us)',
        'LatSeqCreateCreate(us)', 'LatSeqCreateRead(us)', 'LatSeqCreateDelete(us)',
        'LatRandCreateCreate(us)', 'LatRandCreateRead(us)', 'LatRandCreateDelete(us)',
        'message'
        ]
bonnie_test_set = [[2048, 4096], [2048, 6000], [2048, 6250]] #upper bound : 2048 and 6500

bonnie_iter = iter # already the iteration using -x

def bonnie_cmd(param) :
    return "bonnie++ -r " + str(param[0]) + " -s " + str(param[1]) + " -u ubuntu"

def bonnie_process(param, res, res_writer):
    row_csv = [param[0], param[1]]
    result = res.decode('ascii').split("\n")

    if(len(result)-2 < 0) :
        print("ERROR : wrong out put. Got : \n" + str(res))

    result = result[len(result)-2]

    # print(result)

    result = re.sub(r'us', r'', result)
    tab = result.split(',')

    if(len(tab) < 48) :
        print("ERROR : wrong out put. Got : \n" + str(res))

    for e in range(7,19):
        row_csv.append(tab[e])
    for e in range(24,36):
        row_csv.append(tab[e])
    for e in range(36,48):
        row_csv.append(tab[e])
    row_csv.append(result)
    res_writer.writerow(row_csv)

launch_benchmarking(bonnie_filename, bonnie_test_set, bonnie_iter, bonnie_cmd, bonnie_process, bonnie_colnames)
