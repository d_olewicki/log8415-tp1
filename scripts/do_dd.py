from benchmark import *

##### DD #####
dd_filename = 'dd' + '_' + machine
dd_colnames = ['bs', 'count', 'benchmark(MB/s)', 'message']
val_bs = ["500k"] # LIMIT 1M
val_count = ["3250"] # LIMIT 6500
dd_test_set = [["1M", "3k"], ["1M", "5k"]] # cpu_max_print list
dd_iter = iter

def dd_cmd(param) :
    return "dd if=/dev/zero of=sb-io-test bs="+ str(param[0]) + " count=" + str(param[1]) + " conv=fdatasync"

def dd_process(param, res, res_writer):
    result = re.sub(r"(\n)", r" ", res.decode('ascii'))
    pattern = r".*, ([0-9]*\.[0-9]*) MB/s"
    m = re.match(pattern, str(result))
    if(m):
        res_writer.writerow([param[0], param[1], m.group(1), result])
    else:
        res_writer.writerow([param[0], param[1], "NA", result])
        print("ERROR : wrong out put. Got : \n" + str(result))

launch_benchmarking(dd_filename, dd_test_set, dd_iter, dd_cmd, dd_process, dd_colnames)
