from benchmark import *

##### HDPARM #####
hdparm_filename = 'hdparm' + '_' + machine
hdparm_colnames = ['cachedReads(MB/s)', 'bufferedDiskReads(MB/s)', 'message']

hdparm_test_set = [[""]] # no params
hdparm_iter = iter

def hdparm_cmd(param) :
    return "sudo hdparm -Tt /dev/xvda"

def hdparm_process(param, res, res_writer):
    result = re.sub(r"(\n)", r" ", res.decode('ascii'))
    pattern = r".* ([0-9]*\.[0-9]*) MB/sec.* ([0-9]*\.[0-9]*) MB/sec.*"
    m = re.match(pattern, str(result))
    if(m):
        res_writer.writerow([m.group(1), m.group(2), result])
    else:
        res_writer.writerow(["NA", "NA", result])
        print("ERROR : wrong out put. Got : \n" + str(result))

launch_benchmarking(hdparm_filename, hdparm_test_set, hdparm_iter, hdparm_cmd, hdparm_process, hdparm_colnames)
