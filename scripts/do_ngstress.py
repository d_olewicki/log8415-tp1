from benchmark import *

##### NG-STRESS #####
ngstress_filename = 'ngstress' + '_' + machine
ngstress_colnames = ['vm-bytes', 'cpu(bogoOps)', 'io(bogoOps)', 'vm(bogoOps)', 'message']

ngstress_test_set = [["50G"], ["92G"]]
ngstress_iter = iter

def ngstress_cmd(param) :
    return "stress-ng --cpu 4 --io 2 --vm 8192 --vm-bytes " + str(param[0]) + " --timeout 60s --metrics-brief"

def ngstress_process(param, res, res_writer):
    result = re.sub(r"(\n)", r" ", res.decode('ascii'))
    pattern = r".* cpu\s*([0-9]*) .* io\s*([0-9]*) .* vm\s*([0-9]*) .*"
    m = re.match(pattern, str(result))
    if(m):
        res_writer.writerow([param[0], m.group(1), m.group(2), m.group(3), result])
    else:
        res_writer.writerow([param[0],"NA", "NA", "NA", result])
        print("ERROR : wrong out put. Got : \n" + str(result))

launch_benchmarking(ngstress_filename, ngstress_test_set, ngstress_iter, ngstress_cmd, ngstress_process, ngstress_colnames)
