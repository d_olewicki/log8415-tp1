from benchmark import *

##### SPEEDTEST #####
speedtest_filename = 'speedtest' + '_' + machine
speedtest_colnames = ['download(Mbit/s)', 'upload(Mbit/s)', 'message']

speedtest_test_set = [[""]] # no params
speedtest_iter = iter

def speedtest_cmd(param) :
    return "speedtest-cli --simple"

def speedtest_process(param, res, res_writer):
    result = re.sub(r"(\n)", r" ", res.decode('ascii'))
    pattern = r".* ([0-9]*\.[0-9]*) Mbit/s.* ([0-9]*\.[0-9]*) Mbit/s.*"
    m = re.match(pattern, str(result))
    if(m):
        res_writer.writerow([m.group(1), m.group(2), result])
    else:
        res_writer.writerow(["NA", "NA", result])
        print("ERROR : wrong out put. Got : \n" + str(result))

launch_benchmarking(speedtest_filename, speedtest_test_set, speedtest_iter, speedtest_cmd, speedtest_process, speedtest_colnames)
