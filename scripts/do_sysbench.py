from benchmark import *

##### SYSBENCH #####
sysbench_filename = 'sysbench' + '_' + machine
sysbench_colnames = ['num_threads', 'benchmark(ms)', 'message']
sysbench_test_set = [["2000"]] # number of threads
sysbench_iter = iter # already the repetition using --events

def sysbench_cmd(param) :
    return "sysbench --test=cpu --cpu-max-prime=1000000 --events=45000 --time=240 --num_threads=" + str(param[0]) +" run"


def sysbench_process(param, res, res_writer):
    result = re.sub(r"(\n)", r" ", res.decode('ascii'))
    pattern = r".*total number of events:\s*([0-9]*).*avg:\s*([0-9]*\.[0-9]*).*"
    m = re.match(pattern, str(result))
    if(m):
        nbr = m.group(1)
        avg = m.group(2)
        res_writer.writerow([param, avg, "ms", result])
    else:
        res_writer.writerow([e, "NA", result])
        print("ERROR : wrong out put. Got : \n" + str(result))

launch_benchmarking(sysbench_filename, sysbench_test_set, sysbench_iter, sysbench_cmd, sysbench_process, sysbench_colnames)
